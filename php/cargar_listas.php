<?php 
require_once 'conexion.php';

function getListasRep(){
	/*
  $mysqli = getConn();
  $query = 'SELECT * FROM `listas_reproduccion`';
  $result = $mysqli->query($query);
  $listas = '<option value="0">Elige una opción</option>';
  while($row = $result->fetch_array(MYSQLI_ASSOC)){
    $listas .= "<option value='$row[id]'>$row[nombre]</option>";
  }
	 */
	
	if($_POST['accion']=='tipo_canal'){
		$videos = '<option value="0">Elige una canal </option>';
		$videos .= "<option value='1'>OUTBOUND</option>";
		$videos .= "<option value='2'>INBOUND</option>";
		$videos .= "<option value='3'>ASEGURAMIENTO</option>";
		return $videos;
	}
	if($_POST['accion']=='producto'){
		$videos = '<option value="0">Elige un producto</option>';
		$videos .= "<option value='1'>PRIMERA LINEA</option>";
		$videos .= "<option value='2'>SEGUNDA LINEA</option>";
		$videos .= "<option value='3'>MIGRACION</option>";
		$videos .= "<option value='4'>PORTABILIDAD PRE A POST</option>";
		$videos .= "<option value='5'>PORTABILIDAD POST A POST</option>";
		/**/
		$videos .= "<option value='6'>BAM</option>";
		/**/
		$videos .= "<option value='7'>HOGAR</option>";
		$videos .= "<option value='8'>PYME</option>";
		return $videos;
	}
	if($_POST['accion']=='nom_plan'){
		$videos = '<option value="">Elige una Plan</option>';
		$videos .= "<option value=''></option>";
		$videos .= "<option value=''></option>";
		$videos .= "<option value=''></option>";
		return $videos;
	}
	if($_POST['accion']=='eq_mar'){
		$videos = '<option value="0">Elige una Marca</option>';
		$videos .= "<option value='APPLE'>APPLE </option>";
		$videos .= "<option value='SAMSUNG'>SAMSUNG </option>";
		$videos .= "<option value='HUAWEI'>HUAWEI </option>";
		$videos .= "<option value='LG'>LG </option>";
		$videos .= "<option value='MOTOROLA'>MOTOROLA </option>";
		$videos .= "<option value='HTC'>HTC </option>";
		$videos .= "<option value='SONY'>SONY </option>";
		$videos .= "<option value='ALCATEL'>ALCATEL </option>";
		$videos .= "<option value='EQUIPO PROPIO'>EQUIPO PROPIO </option>";
		$videos .= "<option value='MODEM'>MODEM </option>";
		$videos .= "<option value='HOGAR'>HOGAR </option>";
		return $videos;
	}
	if($_POST['accion']=='form_ent'){
		$videos = '<option value="0">Forma de entrega</option>';
		$videos .= "<option value='CAC'>CAC</option>";
		$videos .= "<option value='CPS'>CPS</option>";
		$videos .= "<option value='DAC'>DAC</option>";
		$videos .= "<option value='DELIVERY'>DELIVERY</option>";
		$videos .= "<option value='DELIVERY-CAC'>DELIVERY-CAC</option>";
		return $videos;
	}
	if($_POST['accion']=='tip_acre'){
		$videos = '<option value="0">Seleccione</option>';
		$videos .= "<option value='SIN ACREDITACION'>SIN ACREDITACION</option>";
		$videos .= "<option value='BOLETA COMPETENCIA'>BOLETA COMPETENCIA</option>";
		$videos .= "<option value='CLIENTE CLARO'>CLIENTE CLARO</option>";
		$videos .= "<option value='IVA'>IVA</option>";
		return $videos;
	}
	/*
	if($_POST['accion']=='ciu'){
		$videos = '<option value="0">Seleccione Ciudad</option>';
		$videos .= "<option value='GENERAL CARRERA'>GENERAL CARRERA</option>";
		$videos .= "<option value='COYHAIQUE'>COYHAIQUE</option>";
		$videos .= "<option value='CAPITAN PRAT'>CAPITAN PRAT</option>";
		$videos .= "<option value='AYSEN'>AYSEN</option>";
		$videos .= "<option value='TOCOPILLA'>TOCOPILLA</option>";
		$videos .= "<option value='EL LOA'>EL LOA</option>";
		$videos .= "<option value='ANTOFAGASTA'>ANTOFAGASTA</option>";
		$videos .= "<option value='CALAMA'>CALAMA</option>";
		$videos .= "<option value='PARINACOTA'>PARINACOTA</option>";
		$videos .= "<option value='ARICA'>ARICA</option>";
		$videos .= "<option value='HUASCO'>HUASCO</option>";
		$videos .= "<option value='COPIAPO'>COPIAPO</option>";
		$videos .= "<option value='CHANARAL'>CHANARAL</option>";
		$videos .= "<option value='NUBLE'>NUBLE</option>";
		$videos .= "<option value='CONCEPCION'>CONCEPCION</option>";
		$videos .= "<option value='BIO BIO'>BIO BIO</option>";
		$videos .= "<option value='ARAUCO'>ARAUCO</option>";
		$videos .= "<option value='LIMARI'>LIMARI</option>";
		$videos .= "<option value='ELQUI'>ELQUI</option>";
		$videos .= "<option value='CHOAPA'>CHOAPA</option>";
		$videos .= "<option value='OVALLE'>OVALLE</option>";
		$videos .= "<option value='MALLECO'>MALLECO</option>";
		$videos .= "<option value='CAUTIN'>CAUTIN</option>";
		$videos .= "<option value='COLCHAGUA'>COLCHAGUA</option>";
		$videos .= "<option value='CARDENAL CARO'>CARDENAL CARO</option>";
		$videos .= "<option value='CACHAPOAL'>CACHAPOAL</option>";
		$videos .= "<option value='RANCAGUA'>RANCAGUA</option>";
		$videos .= "<option value='PICHILEMU'>PICHILEMU</option>";
		$videos .= "<option value='PALENA'>PALENA</option>";
		$videos .= "<option value='OSORNO'>OSORNO</option>";
		$videos .= "<option value='LLANQUIHUE'>LLANQUIHUE</option>";
		$videos .= "<option value='CHILOE'>CHILOE</option>";
		$videos .= "<option value='CASTRO'>CASTRO</option>";
		$videos .= "<option value='CHAITEN'>CHAITEN</option>";
		$videos .= "<option value='VALDIVIA'>VALDIVIA</option>";
		$videos .= "<option value='RANCO'>RANCO</option>";
		$videos .= "<option value='ULTIMA ESPERANZA'>ULTIMA ESPERANZA</option>";
		$videos .= "<option value='TIERRA DEL FUEGO'>TIERRA DEL FUEGO</option>";
		$videos .= "<option value='MAGALLANES'>MAGALLANES</option>";
		$videos .= "<option value='ANTARTICA CHILENA'>ANTARTICA CHILENA</option>";
		$videos .= "<option value='TALCA'>TALCA</option>";
		$videos .= "<option value='LINARES'>LINARES</option>";
		$videos .= "<option value='CURICO'>CURICO</option>";
		$videos .= "<option value='CAUQUENES'>CAUQUENES</option>";
		$videos .= "<option value='TALAGANTE'>TALAGANTE</option>";
		$videos .= "<option value='SANTIAGO'>SANTIAGO</option>";
		$videos .= "<option value='MELIPILLA'>MELIPILLA</option>";
		$videos .= "<option value='MAIPO'>MAIPO</option>";
		$videos .= "<option value='CORDILLERA'>CORDILLERA</option>";
		$videos .= "<option value='CHACABUCO'>CHACABUCO</option>";
		$videos .= "<option value='TAMARUGAL'>TAMARUGAL</option>";
		$videos .= "<option value='IQUIQUE'>IQUIQUE</option>";
		$videos .= "<option value='VALPARAISO'>VALPARAISO</option>";
		$videos .= "<option value='SAN FELIPE DE ACONCAGUA'>SAN FELIPE DE ACONCAGUA</option>";
		$videos .= "<option value='SAN ANTONIO'>SAN ANTONIO</option>";
		$videos .= "<option value='QUILLOTA'>QUILLOTA</option>";
		$videos .= "<option value='PETORCA'>PETORCA</option>";
		$videos .= "<option value='MARGA MARGA'>MARGA MARGA</option>";
		$videos .= "<option value='LOS ANDES'>LOS ANDES	</option>";
		return $videos;
	}
	 */
	if($_POST['accion']=='comuna'){
		$videos = '<option value="">Seleccione Comuna</option>';
		$videos .= "<option value='RIO IBANEZ'>RIO IBANEZ</option>";
		$videos .= "<option value='CHILE CHICO'>CHILE CHICO</option>";
		$videos .= "<option value='LAGO VERDE'>LAGO VERDE</option>";
		$videos .= "<option value='COYHAIQUE'>COYHAIQUE</option>";
		$videos .= "<option value='TORTEL'>TORTEL</option>";
		$videos .= "<option value='OHIGGINS'>OHIGGINS</option>";
		$videos .= "<option value='COCHRANE'>COCHRANE</option>";
		$videos .= "<option value='GUAITECAS'>GUAITECAS</option>";
		$videos .= "<option value='CISNES'>CISNES</option>";
		$videos .= "<option value='AYSEN'>AYSEN</option>";
		$videos .= "<option value='TOCOPILLA'>TOCOPILLA</option>";
		$videos .= "<option value='MARIA ELENA'>MARIA ELENA</option>";
		$videos .= "<option value='SAN PEDRO DE ATACAMA'>SAN PEDRO DE ATACAMA</option>";
		$videos .= "<option value='OLLAGUE'>OLLAGUE</option>";
		$videos .= "<option value='CALAMA'>CALAMA</option>";
		$videos .= "<option value='TAL TAL'>TAL TAL</option>";
		$videos .= "<option value='SIERRA GORDA'>SIERRA GORDA</option>";
		$videos .= "<option value='MEJILLONES'>MEJILLONES</option>";
		$videos .= "<option value='ANTOFAGASTA'>ANTOFAGASTA</option>";
		$videos .= "<option value='CHUQUICAMATA'>CHUQUICAMATA</option>";
		$videos .= "<option value='CALAMA'>CALAMA</option>";
		$videos .= "<option value='PUTRE'>PUTRE</option>";
		$videos .= "<option value='GENERAL LAGOS'>GENERAL LAGOS</option>";
		$videos .= "<option value='CAMARONES'>CAMARONES</option>";
		$videos .= "<option value='ARICA'>ARICA</option>";
		$videos .= "<option value='VALLENAR'>VALLENAR</option>";
		$videos .= "<option value='HUASCO'>HUASCO</option>";
		$videos .= "<option value='FREIRINA'>FREIRINA</option>";
		$videos .= "<option value='ALTO DEL CARMEN'>ALTO DEL CARMEN</option>";
		$videos .= "<option value='TIERRA AMARILLA'>TIERRA AMARILLA</option>";
		$videos .= "<option value='COPIAPO'>COPIAPO</option>";
		$videos .= "<option value='CALDERA'>CALDERA</option>";
		$videos .= "<option value='DIEGO DE ALMAGRO'>DIEGO DE ALMAGRO</option>";
		$videos .= "<option value='CHANARAL'>CHANARAL</option>";
		$videos .= "<option value='YUNGAY'>YUNGAY</option>";
		$videos .= "<option value='TREGUACO'>TREGUACO</option>";
		$videos .= "<option value='SAN NICOLAS'>SAN NICOLAS</option>";
		$videos .= "<option value='SAN IGNACIO'>SAN IGNACIO</option>";
		$videos .= "<option value='SAN FABIAN'>SAN FABIAN</option>";
		$videos .= "<option value='SAN CARLOS'>SAN CARLOS</option>";
		$videos .= "<option value='RANQUIL'>RANQUIL</option>";
		$videos .= "<option value='QUIRIHUE'>QUIRIHUE</option>";
		$videos .= "<option value='QUILLON'>QUILLON</option>";
		$videos .= "<option value='PORTEZUELO'>PORTEZUELO</option>";
		$videos .= "<option value='PINTO'>PINTO</option>";
		$videos .= "<option value='PEMUCO'>PEMUCO</option>";
		$videos .= "<option value='NIQUEN'>NIQUEN</option>";
		$videos .= "<option value='NINHUE'>NINHUE</option>";
		$videos .= "<option value='EL CARMEN'>EL CARMEN</option>";
		$videos .= "<option value='COIHUECO'>COIHUECO</option>";
		$videos .= "<option value='COELEMU'>COELEMU</option>";
		$videos .= "<option value='COBQUECURA'>COBQUECURA</option>";
		$videos .= "<option value='CHILLAN VIEJO'>CHILLAN VIEJO</option>";
		$videos .= "<option value='CHILLAN'>CHILLAN </option>";
		$videos .= "<option value='BULNES'>BULNES</option>";
		$videos .= "<option value='TOME'>TOME</option>";
		$videos .= "<option value='TALCAHUANO'>TALCAHUANO</option>";
		$videos .= "<option value='SANTA JUANA'>SANTA JUANA</option>";
		$videos .= "<option value='SAN PEDRO DE LA PAZ'>SAN PEDRO DE LA PAZ</option>";
		$videos .= "<option value='PENCO'>PENCO</option>";
		$videos .= "<option value='LOTA'>LOTA</option>";
		$videos .= "<option value='HUALQUI'>HUALQUI</option>";
		$videos .= "<option value='HUALPEN'>HUALPEN</option>";
		$videos .= "<option value='FLORIDA'>FLORIDA</option>";
		$videos .= "<option value='CORONEL'>CORONEL</option>";
		$videos .= "<option value='CONCEPCION'>CONCEPCION</option>";
		$videos .= "<option value='CHIGUAYANTE'>CHIGUAYANTE</option>";
		$videos .= "<option value='YUMBEL'>YUMBEL</option>";
		$videos .= "<option value='TUCAPEL'>TUCAPEL</option>";
		$videos .= "<option value='SANTA BARBARA'>SANTA BARBARA</option>";
		$videos .= "<option value='SAN ROSENDO'>SAN ROSENDO</option>";
		$videos .= "<option value='QUILLECO'>QUILLECO</option>";
		$videos .= "<option value='QUILLACO'>QUILLACO</option>";
		$videos .= "<option value='NEGRETE'>NEGRETE</option>";
		$videos .= "<option value='NACIMIENTO'>NACIMIENTO</option>";
		$videos .= "<option value='MULCHEN'>MULCHEN</option>";
		$videos .= "<option value='LOS ANGELES'>LOS ANGELES</option>";
		$videos .= "<option value='LAJA'>LAJA</option>";
		$videos .= "<option value='CABRERO'>CABRERO</option>";
		$videos .= "<option value='ANTUCO'>ANTUCO</option>";
		$videos .= "<option value='ALTO BIO BIO'>ALTO BIO BIO</option>";
		$videos .= "<option value='TIRUA'>TIRUA</option>";
		$videos .= "<option value='LOS ALAMOS'>LOS ALAMOS</option>";
		$videos .= "<option value='LEBU'>LEBU</option>";
		$videos .= "<option value='CURANILAHUE'>CURANILAHUE</option>";
		$videos .= "<option value='CONTULMO'>CONTULMO</option>";
		$videos .= "<option value='CANETE'>CANETE</option>";
		$videos .= "<option value='ARAUCO'>ARAUCO</option>";
		$videos .= "<option value='RIO HURTADO'>RIO HURTADO</option>";
		$videos .= "<option value='PUNITAQUI'>PUNITAQUI</option>";
		$videos .= "<option value='MONTE PATRIA'>MONTE PATRIA</option>";
		$videos .= "<option value='COMBARBALA'>COMBARBALA</option>";
		$videos .= "<option value='VICUNA'>VICUNA</option>";
		$videos .= "<option value='PAIGUANO'>PAIGUANO</option>";
		$videos .= "<option value='LA SERENA'>LA SERENA</option>";
		$videos .= "<option value='LA HIGUERA'>LA HIGUERA</option>";
		$videos .= "<option value='COQUIMBO'>COQUIMBO</option>";
		$videos .= "<option value='ANDACOLLO'>ANDACOLLO</option>";
		$videos .= "<option value='SALAMANCA'>SALAMANCA</option>";
		$videos .= "<option value='LOS VILOS'>LOS VILOS</option>";
		$videos .= "<option value='ILLAPEL'>ILLAPEL</option>";
		$videos .= "<option value='CANELA'>CANELA</option>";
		$videos .= "<option value='ALCONES'>ALCONES</option>";
		$videos .= "<option value='OVALLE'>OVALLE</option>";
		$videos .= "<option value='VICTORIA'>VICTORIA</option>";
		$videos .= "<option value='TRAIGUEN'>TRAIGUEN</option>";
		$videos .= "<option value='RENAICO'>RENAICO</option>";
		$videos .= "<option value='PUREN'>PUREN</option>";
		$videos .= "<option value='LUMACO'>LUMACO</option>";
		$videos .= "<option value='LOS SAUCES'>LOS SAUCES</option>";
		$videos .= "<option value='LONQUIMAY'>LONQUIMAY</option>";
		$videos .= "<option value='ERCILLA'>ERCILLA</option>";
		$videos .= "<option value='CURACAUTIN'>CURACAUTIN</option>";
		$videos .= "<option value='COLLIPULLI'>COLLIPULLI</option>";
		$videos .= "<option value='ANGOL'>ANGOL</option>";
		$videos .= "<option value='VILLARRICA'>VILLARRICA</option>";
		$videos .= "<option value='VILCUN'>VILCUN</option>";
		$videos .= "<option value='TOLTEN'>TOLTEN</option>";
		$videos .= "<option value='TEODORO SCHMIDT'>TEODORO SCHMIDT</option>";
		$videos .= "<option value='TEMUCO'>TEMUCO</option>";
		$videos .= "<option value='PUERTO SAAVEDRA'>PUERTO SAAVEDRA</option>";
		$videos .= "<option value='PUCON'>PUCON</option>";
		$videos .= "<option value='PITRUFQUEN'>PITRUFQUEN</option>";
		$videos .= "<option value='PERQUENCO'>PERQUENCO</option>";
		$videos .= "<option value='PADRE LAS CASAS'>PADRE LAS CASAS</option>";
		$videos .= "<option value='NUEVA IMPERIAL'>NUEVA IMPERIAL</option>";
		$videos .= "<option value='PELIPEUCO'>PELIPEUCO</option>";
		$videos .= "<option value='LONCOCHE'>LONCOCHE</option>";
		$videos .= "<option value='LAUTARO'>LAUTARO</option>";
		$videos .= "<option value='GORBEA'>GORBEA</option>";
		$videos .= "<option value='GALVARINO'>GALVARINO</option>";
		$videos .= "<option value='FREIRE'>FREIRE</option>";
		$videos .= "<option value='CURARREHUE'>CURARREHUE</option>";
		$videos .= "<option value='CUNCO'>CUNCO</option>";
		$videos .= "<option value='CHOL CHOL'>CHOL CHOL</option>";
		$videos .= "<option value='CURAHUE'>CURAHUE</option>";
		$videos .= "<option value='SANTA CRUZ'>SANTA CRUZ</option>";
		$videos .= "<option value='SAN FERNANDO'>SAN FERNANDO</option>";
		$videos .= "<option value='PUMANQUE'>PUMANQUE</option>";
		$videos .= "<option value='PLACILLA'>PLACILLA</option>";
		$videos .= "<option value='PERALILLO'>PERALILLO</option>";
		$videos .= "<option value='PALMILLA'>PALMILLA</option>";
		$videos .= "<option value='NANCAGUA'>NANCAGUA</option>";
		$videos .= "<option value='LO LOL'>LO LOL</option>";
		$videos .= "<option value='CHIMBARONGO'>CHIMBARONGO</option>";
		$videos .= "<option value='CHEPICA'>CHEPICA</option>";
		$videos .= "<option value='PAREDONES'>PAREDONES</option>";
		$videos .= "<option value='NAVIDAD'>NAVIDAD</option>";
		$videos .= "<option value='MARCHIHUE'>MARCHIHUE</option>";
		$videos .= "<option value='LITUECHE'>LITUECHE</option>";
		$videos .= "<option value='LA ESTRELLA'>LA ESTRELLA</option>";
		$videos .= "<option value='SAN VICENTE'>SAN VICENTE</option>";
		$videos .= "<option value='REQUINOA'>REQUINOA</option>";
		$videos .= "<option value='RENGO'>RENGO</option>";
		$videos .= "<option value='QUINTA DE TILCOCO'>QUINTA DE TILCOCO</option>";
		$videos .= "<option value='PICHIDEGUA'>PICHIDEGUA</option>";
		$videos .= "<option value='PEUMO'>PEUMO</option>";
		$videos .= "<option value='OLIVAR'>OLIVAR</option>";
		$videos .= "<option value='MOSTAZAL'>MOSTAZAL</option>";
		$videos .= "<option value='MALLOA'>MALLOA</option>";
		$videos .= "<option value='MACHALI'>MACHALI</option>";
		$videos .= "<option value='LAS CABRAS'>LAS CABRAS</option>";
		$videos .= "<option value='GRANEROS'>GRANEROS</option>";
		$videos .= "<option value='DONIHUE'>DONIHUE</option>";
		$videos .= "<option value='COLTAUCO'>COLTAUCO</option>";
		$videos .= "<option value='COINCO'>COINCO</option>";
		$videos .= "<option value='CODEGUA'>CODEGUA</option>";
		$videos .= "<option value='GULTRO'>GULTRO</option>";
		$videos .= "<option value='LOS LIRIOS'>LOS LIRIOS</option>";
		$videos .= "<option value='RANCAGUA'>RANCAGUA</option>";
		$videos .= "<option value='BUCALEMU'>BUCALEMU</option>";
		$videos .= "<option value='PICHILEMU'>PICHILEMU</option>";
		$videos .= "<option value='PALENA'>PALENA</option>";
		$videos .= "<option value='HUALAIHUE'>HUALAIHUE</option>";
		$videos .= "<option value='FUTALEUFU'>FUTALEUFU</option>";
		$videos .= "<option value='SAN PABLO'>SAN PABLO</option>";
		$videos .= "<option value='SAN JUAN DE LA COSTA'>SAN JUAN DE LA COSTA</option>";
		$videos .= "<option value='RIO NEGRO'>RIO NEGRO</option>";
		$videos .= "<option value='PUYEHUE'>PUYEHUE</option>";
		$videos .= "<option value='PURRANQUE'>PURRANQUE</option>";
		$videos .= "<option value='PUERTO OCTAY'>PUERTO OCTAY</option>";
		$videos .= "<option value='OSORNO'>OSORNO</option>";
		$videos .= "<option value='ENTRE LAGOS'>ENTRE LAGOS</option>";
		$videos .= "<option value='PUAUCHO'>PUAUCHO</option>";
		$videos .= "<option value='PUERTO VARAS'>PUERTO VARAS</option>";
		$videos .= "<option value='PUERTO MONTT'>PUERTO MONTT</option>";
		$videos .= "<option value='MAULLIN'>MAULLIN</option>";
		$videos .= "<option value='LOS MUERMOS'>LOS MUERMOS</option>";
		$videos .= "<option value='LLANQUIHUE'>LLANQUIHUE</option>";
		$videos .= "<option value='FRUTILLAR'>FRUTILLAR</option>";
		$videos .= "<option value='FRESIA'>FRESIA</option>";
		$videos .= "<option value='COCHAMO'>COCHAMO</option>";
		$videos .= "<option value='CALBUCO'>CALBUCO</option>";
		$videos .= "<option value='QUINCHAO'>QUINCHAO</option>";
		$videos .= "<option value='CHILOE'>CHILOE</option>";
		$videos .= "<option value='QUEMCHI'>QUEMCHI</option>";
		$videos .= "<option value='QUELLON'>QUELLON</option>";
		$videos .= "<option value='QUEILEN'>QUEILEN</option>";
		$videos .= "<option value='PUQUELDON'>PUQUELDON</option>";
		$videos .= "<option value='DALCAHUE'>DALCAHUE</option>";
		$videos .= "<option value='CURACO DE VELEZ'>CURACO DE VELEZ</option>";
		$videos .= "<option value='CHONCHI'>CHONCHI</option>";
		$videos .= "<option value='ANCUD'>ANCUD</option>";
		$videos .= "<option value='CASTRO'>CASTRO</option>";
		$videos .= "<option value='CORCOVADO'>CORCOVADO</option>";
		$videos .= "<option value='CHAITEN'>CHAITEN</option>";
		$videos .= "<option value='VALDIVIA'>VALDIVIA</option>";
		$videos .= "<option value='PANGUIPULLI'>PANGUIPULLI</option>";
		$videos .= "<option value='PAILLACO'>PAILLACO</option>";
		$videos .= "<option value='MARIQUINA'>MARIQUINA</option>";
		$videos .= "<option value='MAFIL'>MAFIL</option>";
		$videos .= "<option value='LOS LAGOS'>LOS LAGOS</option>";
		$videos .= "<option value='LANCO'>LANCO</option>";
		$videos .= "<option value='CORRAL'>CORRAL</option>";
		$videos .= "<option value='RIO BUENO'>RIO BUENO</option>";
		$videos .= "<option value='LAGO RANCO'>LAGO RANCO</option>";
		$videos .= "<option value='LA UNION'>LA UNION</option>";
		$videos .= "<option value='FUTRONO'>FUTRONO</option>";
		$videos .= "<option value='TORRES DEL PAINE'>TORRES DEL PAINE</option>";
		$videos .= "<option value='NATALES'>NATALES</option>";
		$videos .= "<option value='PRIMAVERA'>PRIMAVERA</option>";
		$videos .= "<option value='TIMAUKEL'>TIMAUKEL</option>";
		$videos .= "<option value='PORVENIR'>PORVENIR</option>";
		$videos .= "<option value='SAN GREGORIO'>SAN GREGORIO</option>";
		$videos .= "<option value='RIO VERDE'>RIO VERDE</option>";
		$videos .= "<option value='PUNTA ARENAS'>PUNTA ARENAS</option>";
		$videos .= "<option value='LAGUNA BLANCA'>LAGUNA BLANCA</option>";
		$videos .= "<option value='CABO DE HORNOS'>CABO DE HORNOS</option>";
		$videos .= "<option value='ANTARTICA'>ANTARTICA</option>";
		$videos .= "<option value='TALCA'>TALCA</option>";
		$videos .= "<option value='SAN RAFAEL'>SAN RAFAEL</option>";
		$videos .= "<option value='SAN CLEMENTE'>SAN CLEMENTE</option>";
		$videos .= "<option value='RIO CLARO'>RIO CLARO</option>";
		$videos .= "<option value='PENCAHUE'>PENCAHUE</option>";
		$videos .= "<option value='PELARCO'>PELARCO</option>";
		$videos .= "<option value='MAULE'>MAULE</option>";
		$videos .= "<option value='EMPEDRADO'>EMPEDRADO</option>";
		$videos .= "<option value='CUREPTO'>CUREPTO</option>";
		$videos .= "<option value='CONSTITUCION'>CONSTITUCION</option>";
		$videos .= "<option value='YERBAS BUENAS'>YERBAS BUENAS</option>";
		$videos .= "<option value='VILLA ALEGRE'>VILLA ALEGRE</option>";
		$videos .= "<option value='SAN JAVIER'>SAN JAVIER</option>";
		$videos .= "<option value='RETIRO'>RETIRO</option>";
		$videos .= "<option value='PARRAL'>PARRAL</option>";
		$videos .= "<option value='LONGAVI'>LONGAVI</option>";
		$videos .= "<option value='LINARES'>LINARES</option>";
		$videos .= "<option value='COLBUN'>COLBUN</option>";
		$videos .= "<option value='VICHUQUEN'>VICHUQUEN</option>";
		$videos .= "<option value='TENO'>TENO</option>";
		$videos .= "<option value='SAGRADA FAMILIA'>SAGRADA FAMILIA</option>";
		$videos .= "<option value='ROMERAL'>ROMERAL</option>";
		$videos .= "<option value='RAUCO'>RAUCO</option>";
		$videos .= "<option value='MOLINA'>MOLINA</option>";
		$videos .= "<option value='LICANTEN'>LICANTEN</option>";
		$videos .= "<option value='HUALANE'>HUALANE</option>";
		$videos .= "<option value='CURICO'>CURICO</option>";
		$videos .= "<option value='PELLUHUE'>PELLUHUE</option>";
		$videos .= "<option value='CHANCO'>CHANCO</option>";
		$videos .= "<option value='CAUQUENES'>CAUQUENES</option>";
		$videos .= "<option value='TALAGANTE'>TALAGANTE</option>";
		$videos .= "<option value='PENAFLOR'>PENAFLOR</option>";
		$videos .= "<option value='PADRE HURTADO'>PADRE HURTADO</option>";
		$videos .= "<option value='ISLA DE MAIPO'>ISLA DE MAIPO</option>";
		$videos .= "<option value='EL MONTE'>EL MONTE</option>";
		$videos .= "<option value='SANTIAGO'>SANTIAGO</option>";
		$videos .= "<option value='VITACURA'>VITACURA</option>";
		$videos .= "<option value='SAN RAMON'>SAN RAMON</option>";
		$videos .= "<option value='SAN MIGUEL'>SAN MIGUEL</option>";
		$videos .= "<option value='SAN JOAQUIN'>SAN JOAQUIN</option>";
		$videos .= "<option value='QUILICURA'>QUILICURA</option>";
		$videos .= "<option value='RENCA'>RENCA </option>";
		$videos .= "<option value='RECOLETA'>RECOLETA</option>";
		$videos .= "<option value='QUINTA NORMAL'>QUINTA NORMAL</option>";
		$videos .= "<option value='PROVIDENCIA'>PROVIDENCIA</option>";
		$videos .= "<option value='PADAHUEL'>PADAHUEL</option>";
		$videos .= "<option value='MACUL'>MACUL</option>";
		$videos .= "<option value='MAIPU'>MAIPU</option>";
		$videos .= "<option value='PENALOLEN'>PENALOLEN</option>";
		$videos .= "<option value='PEDRO AGUIRRE CERDA'>PEDRO AGUIRRE CERDA</option>";
		$videos .= "<option value='NUNOA'>NUNOA </option>";
		$videos .= "<option value='LO BARNECHEA'>LO BARNECHEA</option>";
		$videos .= "<option value='LO PRADO'>LO PRADO</option>";
		$videos .= "<option value='LO ESPEJO'>LO ESPEJO</option>";
		$videos .= "<option value='LA FLORIDA'>LA FLORIDA</option>";
		$videos .= "<option value='LAS CONDES'>LAS CONDES</option>";
		$videos .= "<option value='LA REINA'>LA REINA</option>";
		$videos .= "<option value='LA PINTANA'>LA PINTANA</option>";
		$videos .= "<option value='LA GRANJA'>LA GRANJA</option>";
		$videos .= "<option value='CERRILLOS'>CERRILLOS</option>";
		$videos .= "<option value='LA CISTERNA'>LA CISTERNA</option>";
		$videos .= "<option value='INDEPENDENCIA'>INDEPENDENCIA</option>";
		$videos .= "<option value='HUECHURABA'>HUECHURABA</option>";
		$videos .= "<option value='ESTACION CENTRAL'>ESTACION CENTRAL</option>";
		$videos .= "<option value='EL BOSQUE'>EL BOSQUE</option>";
		$videos .= "<option value='CONCHALI'>CONCHALI</option>";
		$videos .= "<option value='CERRO NAVIA'>CERRO NAVIA</option>";
		$videos .= "<option value='NATANIEL'>NATANIEL</option>";
		$videos .= "<option value='MALLOCO'>MALLOCO</option>";
		$videos .= "<option value='SAN PEDRO'>SAN PEDRO</option>";
		$videos .= "<option value='MELIPILLA'>MELIPILLA</option>";
		$videos .= "<option value='MARIA PINTO'>MARIA PINTO</option>";
		$videos .= "<option value='CURACAVI'>CURACAVI</option>";
		$videos .= "<option value='ALHUE'>ALHUE</option>";
		$videos .= "<option value='SAN BERNARDO'>SAN BERNARDO</option>";
		$videos .= "<option value='PAINE'>PAINE</option>";
		$videos .= "<option value='CALERA DE TANGO'>CALERA DE TANGO</option>";
		$videos .= "<option value='BUIN'>BUIN</option>";
		$videos .= "<option value='SAN JOSE DE MAIPO'>SAN JOSE DE MAIPO</option>";
		$videos .= "<option value='PUENTE ALTO'>PUENTE ALTO</option>";
		$videos .= "<option value='PIRQUE'>PIRQUE</option>";
		$videos .= "<option value='TILTIL'>TILTIL</option>";
		$videos .= "<option value='LAMPA'>LAMPA</option>";
		$videos .= "<option value='COLINA'>COLINA</option>";
		$videos .= "<option value='POZO ALMONTE'>POZO ALMONTE</option>";
		$videos .= "<option value='PICA'>PICA</option>";
		$videos .= "<option value='HUARA'>HUARA</option>";
		$videos .= "<option value='COLCHANE'>COLCHANE</option>";
		$videos .= "<option value='CAMINA'>CAMINA</option>";
		$videos .= "<option value='IQUIQUE'>IQUIQUE</option>";
		$videos .= "<option value='ALTO HOSPICIO'>ALTO HOSPICIO</option>";
		$videos .= "<option value='VINA DEL MAR'>VINA DEL MAR</option>";
		$videos .= "<option value='VALPARAISO'>VALPARAISO</option>";
		$videos .= "<option value='QUINTERO'>QUINTERO</option>";
		$videos .= "<option value='PUCHANCAVI'>PUCHANCAVI</option>";
		$videos .= "<option value='CONCON'>CONCON</option>";
		$videos .= "<option value='JUAN FERNANDEZ'>JUAN FERNANDEZ</option>";
		$videos .= "<option value='CASABLANCA'>CASABLANCA</option>";
		$videos .= "<option value='SANTA MARIA'>SANTA MARIA</option>";
		$videos .= "<option value='SAN FELIPE'>SAN FELIPE</option>";
		$videos .= "<option value='PUTAENDO'>PUTAENDO</option>";
		$videos .= "<option value='PANQUEHUE'>PANQUEHUE</option>";
		$videos .= "<option value='LLAILLAY'>LLAILLAY</option>";
		$videos .= "<option value='CATEMU'>CATEMU</option>";
		$videos .= "<option value='SANTO DOMINGO'>SANTO DOMINGO</option>";
		$videos .= "<option value='SAN ANTONIO'>SAN ANTONIO</option>";
		$videos .= "<option value='EL TABO'>EL TABO</option>";
		$videos .= "<option value='EL QUISCO'>EL QUISCO</option>";
		$videos .= "<option value='CARTAGENA'>CARTAGENA</option>";
		$videos .= "<option value='ALGARROBO'>ALGARROBO</option>";
		$videos .= "<option value='QUILLOTA'>QUILLOTA</option>";
		$videos .= "<option value='NOGALES'>NOGALES</option>";
		$videos .= "<option value='HIJUELAS'>HIJUELAS</option>";
		$videos .= "<option value='LA CRUZ'>LA CRUZ</option>";
		$videos .= "<option value='LA CALERA'>LA CALERA</option>";
		$videos .= "<option value='ZAPALLAR'>ZAPALLAR</option>";
		$videos .= "<option value='PETORCA'>PETORCA</option>";
		$videos .= "<option value='PAPUDO'>PAPUDO</option>";
		$videos .= "<option value='LA LIGUA'>LA LIGUA</option>";
		$videos .= "<option value='CABILDO'>CABILDO</option>";
		$videos .= "<option value='QUILPUE'>QUILPUE</option>";
		$videos .= "<option value='VILLA ALEMANA'>VILLA ALEMANA</option>";
		$videos .= "<option value='OLMUE'>OLMUE</option>";
		$videos .= "<option value='LIMACHE'>LIMACHE</option>";
		$videos .= "<option value='SAN ESTEBAN'>SAN ESTEBAN</option>";
		$videos .= "<option value='RINCONADA'>RINCONADA</option>";
		$videos .= "<option value='LOS ANDES'>LOS ANDES</option>";
		$videos .= "<option value='CALLE LARGA'>CALLE LARGA</option>";
		$videos .= "<option value='LOS ANDES'>LOS ANDES</option>";
		$videos .= "<option value='ISLA DE PASCUA'>ISLA DE PASCUA</option>";
		return $videos;
	}
}

echo getListasRep();
