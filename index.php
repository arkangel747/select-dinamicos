<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Cómo hacer un select dinámico con Php y Jquery</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="css/index.css">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="page-header text-left">
      <h1> Registro Unificado de Ventas <small>Discador y Sistema de ventas</small></h1>
    </div>
        <form id="formulario" action="insert.php" method="post"><!-- onsubmit="return validaFormulario()">-->
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<label>Numero Caso</label>
					<label id='num_casoe' class="labelA hide" for="num_caso">*Debe tener 9 caracteres</label>
					<label id='num_casoe1' class="labelA hide" for="num_caso">*Debe tener 9 caracteres Empezando con C</label>
					<input id="num_caso" name="num_caso" type="text" class="form-control" required />
				</div>
				<div class="col-md-4">
					<label>Numero Orden</label>
					<label id='num_orde' class="labelA hide" for="num_ord">*Debe tener 11 caracteres Terminando con A</label>
					<input id="num_ord" name="num_ord" type="text" class="form-control" required />
				</div>
				<div class="col-md-4">
					<label>PCS a Portar</label>
					<label id="pcsporte" class="labelA hide" for="pcsport">*Debe tener 11 caracteres Empezando con 56</label>
					<label id="pcsporta" class="labelB hide" for="pcsport"></label>
					<input id="pcsport" name="pcsport" type="text" class="form-control" required />
				</div>
			</div>
		</div>
	</div>
        <div class="row">
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Nombre Cliente</label>
					<label id="nombe" class="labelA hide" for="nomb">*Debe ser solo letras</label>
					<input id="nomb" name="nomb" type="text" class="form-control" required />
                                </div>
                                <div class="col-md-4">
					<label>Apellido Clente</label>
					<label id="apelle" class="labelA hide" for="apell">*Debe ser solo letras</label>
					<input id="apell" name="apell" type="text" class="form-control" required />
                                </div>
                                <div class="col-md-4">
					<label>Rut Cliente</label>
					<label id="rutce" class="labelA hide" for="rutc">*Formato de rut no valido</label>
					<input id="rutc" name="rutc" type="text" class="form-control" required />
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>PCS Contactado</label>
					<label id="pcsconte" class="labelA hide" for="pcscont">*Debe tener 11 caracteres Empezando con 56</label>
					<label id="pcsconta" class="labelB hide" for="pcscont"></label>
					<input id="pcscont" name="pcscont" type="text" class="form-control" required />
                                </div>
                                <div class="col-md-4">
					<label>Tipo Canal</label>
					<select id="tipo_canal" name="tipo_canal" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Campaña</label>
					<select id="camp" name="camp" class="form-control" required >          </select>
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Producto Ofrecido</label>
                                        <select id="producto" name="producto" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Nombre Plan</label>
                                        <select id="nom_plan" name="nom_plan" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Codigo Plan</label>
					<input id="cod_plan" name="cod_plan" type="text" class="form-control" disabled />
					<input type="hidden" name="cod_planh" id="cod_planh" value="">
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Equipo Marca</label>
                                        <select id="eq_mar" name="eq_mar" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Equipo Modelo</label>
                                        <select id="eq_mod" name="eq_mod" class="form-control" required>          </select>
                                </div>
                                <div class="col-md-4">
					<label>Codigo Equipo</label>
					<input id="cod_eq" name="cod_eq" type="text" class="form-control" disabled />
					<input type="hidden" name="cod_eqh" id="cod_eqh" value="">
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Forma Entrega</label>
                                        <select id="form_ent" name="form_ent" class="form-control" required>          </select>
                                </div>
                                <div class="col-md-4">
					<label>Destino Entrega</label>
                                        <select id="dest_ent" name="dest_ent" class="form-control" required>          </select>
                                </div>
                                <div class="col-md-4">
					<label>Region Cac Delivery</label>
					<input id="reg_cac_d" name="reg_cac_d" type="text" class="form-control" disabled />
					<input type="hidden" name="reg_cac_dh" id="reg_cac_dh" value="">
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Codigo Sucursal</label>
					<input id="cod_suc" name="cod_suc" type="text" class="form-control" disabled />
					<input type="hidden" name="cod_such" id="cod_such" value="">
                                </div>
                                <div class="col-md-4">
					<label>Tipo Acreditacion</label>
                                        <select id="tip_acre" name="tip_acre" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Abono</label>
                                        <select id="abono" name="abono" class="form-control" required >          </select>
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Monto Abono</label>
                                        <select id="mont_abo" name="mont_abo" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Comuna</label>
                                        <select id="comuna" name="comuna" class="form-control" required >          </select>
                                </div>
                                <div class="col-md-4">
					<label>Ciudad</label>
					<input id="ciudad" name="ciudad" type="text" class="form-control" disabled />
					<input type="hidden" name="ciudadh" id="ciudadh" value="">
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
					<label>Direccion Ingreso One</label>
					<input id="dir_ing_one" name="dir_ing_one" type="text" class="form-control" required />
                                </div>
                                <div class="col-md-4">
					<label>Direccion Entrega</label>
					<input id="dir_ent" name="dir_ent" type="text" class="form-control" />
                                </div>
                                <div class="col-md-4">
                                </div>
                        </div>
		</div>
                <div class="col-md-12">
                        <div class="row">
                                <div class="col-md-4">
                                </div>
				<div class="col-md-4">
                                         <input id="enviar" name="enviar" type="submit" value="Enviar" />
                                </div>
                                <div class="col-md-4">
                                </div>
                        </div>
		</div>
        </div>
        </form>
</div>
<!--
  <div class="container">
    <div class="alert alert-info" role="alert">Ejemplo de 2 selects</div>
    <p>Elige un vídeo según la lista de reproducción:</p>
    <div class="row">
      <div class="col-md-4">
          <p>Lista de reproducción
          <select id="lista_reproduccion" name="lista_reproduccion" class="form-control">
          </select>
        </p>
      </div>
      <div class="col-md-4">
        <p>Vídeos
        <select id="videos" name="video" class="form-control">
        </select>
      </p>
      </div>
      <div class="col-md-4">
        <p><br><button id="enviar" type="submit" class="btn btn-default btn-block">Enviar</button></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <p><b>El resultado es: </b></p><p id="resultado1"></p>
      </div>
    </div>
  </div>
-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
  </script>
  <script type="text/javascript" src="js/index.js"></script>
</body>
</html>
